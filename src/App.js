import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
import cities from './cities-fr.json';
import './loader.css';
var apikey = '46e5b876fb0548c238b9755a7d8ed010';
function App() {
  const [value, setValue] = useState('');
  const [jsObj, setJsObj] = useState('');
  function handleChange(e) {
    let data = cities.filter((item, index, arr) => {
      return item.nm == e.target.value
    })
    data = data && data[0];

    setValue(e.target.value)

    var xhr = new XMLHttpRequest();
    if(!data){
      setJsObj(undefined)
      return;
    }
    // var apiCall = "https://api.openweathermap.org/data/2.5/weather?lat=" + data.lat + "&lon=" + data.lon + "&appid=9d06d9b4825f10f79591ff063769f070";
    var apiCall = `http://api.openweathermap.org/data/2.5/onecall?lat=${data.lat}&lon=${data.lon}&appid=9d06d9b4825f10f79591ff063769f070&units=metric&lang=en`
    xhr.open("GET", apiCall, false);
    xhr.send();

    let jsObj = JSON.parse(xhr.response);
    jsObj.daily.map((day, i) => {
      let dt = new Date(day.dt * 1000);
      let d = dt.toDateString().split(' ');
    })
    setJsObj(jsObj)

  }

  return (
    <div className="main">
      <div className='header'>Selectionner votre ville</div>
      <select value={value} onChange={(e) => handleChange(e)}>
        <option value="">select</option>
        {cities && cities.map((data, i) => <option key={i} value={data.nm}>{data.nm}</option>)}
      </select>
      {
        !jsObj && <div className="loader"></div>
      }
      {
        jsObj &&
        <>
          <div className="cityMain">
            <span className='city'>{value}</span>
          </div>
          <div className='image'>
            <img src={`http://openweathermap.org/img/wn/${jsObj && jsObj.daily && jsObj.current.weather[0].icon
              }@4x.png`} alt="logo" />
            <div className='degree'>{jsObj && jsObj.current.temp}</div>
          </div>
          <div className='weekMain'>
            {jsObj && jsObj.daily && jsObj.daily.map((day, i) => (
              i < 3 && <div className='week' key={i}>
                <div className='weekday'>{new Date(day.dt * 1000).toDateString().split(' ')[0]}</div>
                <img src={`http://openweathermap.org/img/wn/${day.weather[0].icon
                  }@4x.png`} className="weekimg" alt="logo" />
                <div className='number'>{day.temp.max}</div>
                <div className='number'>{day.temp.min}</div>
              </div>
            ))}
          </div>
        </>
      }
    </div>
  );
}

export default App;
